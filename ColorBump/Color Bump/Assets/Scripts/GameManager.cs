﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager singleton;
    public bool GameStarted { get; private set; }
    public bool GameEnded { get; private set; }

    [SerializeField] private float slowMotionFactor = 0.1f;
    [SerializeField] Transform startTranform;
    [SerializeField] Transform goalTranform;
    [SerializeField] BallController ball;

    public float EntireDistance { get; private set; }
    public float DistanceLeft { get; private set; }

    private void Start()
    {
        EntireDistance = goalTranform.position.z - startTranform.position.z;
    }

    void Awake()
    {
        if(singleton == null)
        {
            singleton = this;
        }else if(singleton != this)
        {
            Destroy(gameObject);
        }
        Time.timeScale = 1f;
        Time.fixedDeltaTime = 0.02f;
    }

    public void StarGame()
    {
        GameStarted = true;
        Debug.Log("GameStar");
    }

    public void EndGame(bool win)
    {
        GameEnded = true;
        Debug.Log("GameEnd");

        if (!win)
        {
            //restart game
            Invoke("RestartGame", 2 * slowMotionFactor);
            Time.timeScale = slowMotionFactor;
            Time.fixedDeltaTime = 0.02f * Time.timeScale;
        }
        else
        {
            Invoke("RestartGame", 2);
        }
    }

    public void RestartGame()
    {
        UnityEngine.SceneManagement.SceneManager.LoadScene(0);
    }
    // Update is called once per frame
    void Update()
    {
        DistanceLeft = Vector3.Distance(ball.transform.position, goalTranform.position);

        if (DistanceLeft > EntireDistance)
            DistanceLeft = EntireDistance;

        if (ball.transform.position.z > goalTranform.transform.position.z)
            DistanceLeft = 0;

        //Debug.Log("Travel distance is " + DistanceLeft + " EntireDistance is " + EntireDistance);
    }
}
