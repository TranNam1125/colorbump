﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    //public GameObject player;
    //private Vector3 offset;

    //private void Start()
    //{
    //    offset = transform.position - player.transform.position;
    //}

    //private void LateUpdate()
    //{
        
    //    transform.position = player.transform.position + offset;
        
    //}
    private void FixedUpdate()
    {
        if (GameManager.singleton.GameStarted)
            transform.position = transform.position + Vector3.forward * 5 * Time.fixedDeltaTime;
    }
}
